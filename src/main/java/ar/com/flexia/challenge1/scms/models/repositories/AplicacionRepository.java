package ar.com.flexia.challenge1.scms.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.flexia.challenge1.scms.models.Aplicacion;

@Repository
public interface AplicacionRepository extends JpaRepository<Aplicacion, Long> {

}
