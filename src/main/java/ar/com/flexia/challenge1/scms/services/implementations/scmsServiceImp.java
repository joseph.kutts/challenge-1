package ar.com.flexia.challenge1.scms.services.implementations;

import ar.com.flexia.challenge1.auth.models.APITokens;
import ar.com.flexia.challenge1.global.dtos.APIConfirmation;
import ar.com.flexia.challenge1.global.dtos.APIException;
import ar.com.flexia.challenge1.scms.models.Ambiente;
import ar.com.flexia.challenge1.scms.models.Aplicacion;
import ar.com.flexia.challenge1.scms.models.Registro;
import ar.com.flexia.challenge1.scms.models.dtos.DeleteKeys;
import ar.com.flexia.challenge1.scms.models.dtos.ModifiedKeys;
import ar.com.flexia.challenge1.scms.models.repositories.AmbienteRepository;
import ar.com.flexia.challenge1.scms.models.repositories.AplicacionRepository;
import ar.com.flexia.challenge1.scms.models.repositories.RegistroRepository;
import ar.com.flexia.challenge1.scms.services.scmsService;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class scmsServiceImp implements scmsService {

    @Autowired
    private AplicacionRepository appRepo;

    @Autowired
    private AmbienteRepository envRepo;

    @Autowired
    private RegistroRepository regRepo;

    @Autowired
    private PasswordEncoder encoder;

    public scmsServiceImp() {}

    private Aplicacion checkApp(Long appId) {
        Optional<Aplicacion> app = appRepo.findById(appId);

        app.orElseThrow(() -> {
            return new APIException("400", "La aplicacion no se encuentra en la base de datos.");
        });

        return app.get();
    }

    private Ambiente checkEnv(Long envId) {
        Optional<Ambiente> env = envRepo.findById(envId);

        env.orElseThrow(() -> {
            return new APIException("400", "El entorno no se encuentra en la base de datos.");
        });

        return env.get();
    }


    @Override
    public APIConfirmation setApp(Aplicacion app) {

        Aplicacion finalObject = appRepo.save(app);

        return new APIConfirmation("200", "Aplicación guardada en la base de datos.", finalObject);
    }

    @Override
    public APIConfirmation setEnv(Long appId, Ambiente env)     {
        Aplicacion app = checkApp(appId);

        String secret = UUID.randomUUID().toString();
        APITokens tokens = new APITokens(app.getNombre(), UUID.randomUUID().toString(), encoder.encode(secret), "CLIENT");

        env.setTokens(tokens);
        env.setAplicacion(app);

        Ambiente finalObject = envRepo.save(env);

        // Devolvemos el valor del Secret token por unica vez
        finalObject.getTokens().setSecret(secret);

        return new APIConfirmation("200", "Aplicación guardada en la base de datos.", finalObject);
    }

    @Override
    public APIConfirmation setConfig(Long envId, Map<String, String> config) {
        Ambiente env = checkEnv(envId);

        List<Registro> registros = regRepo.findByAmbiente_Id(envId);
        List<String> keys = registros.stream().parallel().map((reg) -> { return reg.getKey(); }).collect(Collectors.toList());
        List<String> modifiedKeys = new ArrayList<>();

        config.forEach((k, v) -> {
            if (!keys.contains(k)) {
                Registro registro = new Registro(k, v, env);
                regRepo.save(registro);
                modifiedKeys.add(k);
            }
        });

        if (modifiedKeys.isEmpty()) {
            throw new APIException("400", "No se agrego ningun registro de configuración. Todos los campos ya se encontraban guardados, te recomendamos intentaron con el metodo PUT para editarlos.");
        }

        return new APIConfirmation("200", "Todos los registros de configuración nuevos fueron guardados");
    }

    @Override
    public APIConfirmation editConfig(Long envId, Map<String, String> config) {
        List<Registro> registros = regRepo.findByAmbiente_Id(envId);
        List<String> keys = registros.stream().parallel().map((reg) -> { return reg.getKey(); }).collect(Collectors.toList());
        ModifiedKeys modifiedKeys = new ModifiedKeys();

        config.forEach((k, v) -> {
            if (keys.contains(k)) { // Se podria omitir, no sabria decir que es más rapido
                Optional<Registro> registro = registros.stream().parallel().filter((reg) -> { return reg.getKey().equals(k); }).findFirst();

                registro.orElseThrow(() -> {
                    return new APIException("400", "El registro que se intenta modificar no pertenece al ambiente especificado.");
                });

                registro.get().setValue(v);

                regRepo.save(registro.get());
                modifiedKeys.addModificado(k);
            }
        });

        if (modifiedKeys.isEmpty()) {
            throw new APIException("400", "No se modifico ningun registro de configuración. Ningun campos se encontraba guardado, te recomendamos intentaron con el metodo POST para agregarlos.");
        }

        return new APIConfirmation("200", "Registros existentes editados correctamente.", modifiedKeys);
    }

    @Override
    @Transactional
    public APIConfirmation deleteConfig(Long envId, DeleteKeys deleteList) {
        ModifiedKeys modifiedKeys = new ModifiedKeys();

        if (deleteList.isEmpty()) {
            regRepo.deleteAllByAmbiente_Id(envId);
        } else {
            deleteList.getCampos().forEach((campo) -> {
                regRepo.deleteByKeyAndAmbiente_Id(campo, envId);
                modifiedKeys.addModificado(campo);
            });
        }

        if (modifiedKeys.isEmpty()) {
            return new APIConfirmation("200", "Todos los campos fueron eliminados.");
        } else {
            return new APIConfirmation("200", "Los campos indicados fueron eliminados.", modifiedKeys);
        }
    }

    @Override
    public Map<String, String> getConfig(Long envId) {
        Map<String, String> config = new HashMap<String, String>();

        List<Registro> registros = regRepo.findByAmbiente_Id(envId);

        registros.forEach((registro) -> {
            config.put(registro.getKey(), registro.getValue());
        });

        return config;
    }
}
