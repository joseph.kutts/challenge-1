package ar.com.flexia.challenge1.scms.services.implementations;

import ar.com.flexia.challenge1.global.dtos.APIException;
import ar.com.flexia.challenge1.scms.models.Ambiente;
import ar.com.flexia.challenge1.scms.models.repositories.AmbienteRepository;
import ar.com.flexia.challenge1.scms.models.repositories.AplicacionRepository;
import org.hibernate.cfg.Environment;
import org.springframework.aop.framework.AopInfrastructureBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component(value="APIVerification")
public class APIVerificationImp implements AopInfrastructureBean {

    @Autowired
    private AmbienteRepository envRepo;

    public APIVerificationImp() {}

    private Ambiente checkEnv(Long envId) {
        Optional<Ambiente> env = envRepo.findById(envId);

        env.orElseThrow(() -> {
            return new APIException("400", "El entorno no se encuentra en la base de datos.");
        });

        return env.get();
    }

    public boolean checkAmbienteWithAplicacion(Long envId, Long appid) {
        Ambiente env = checkEnv(envId);

        if (!env.getAplicacion().getId().equals(appid)) {
            throw new APIException("400", "El ambiente indicado no pertenece a la aplicación.");
        }

        return true;
    }

}

