package ar.com.flexia.challenge1.scms.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Registro {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ambiente")
    @JsonIgnore
    private Long id;

    private String key;

    private String value;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ambiente.id")
    @JsonIgnore
    private Ambiente ambiente;

    public Registro() {}

    public Registro(String key, String value, Ambiente ambiente) {
        this.key = key;
        this.value = value;
        this.ambiente = ambiente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Ambiente getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(Ambiente ambiente) {
        this.ambiente = ambiente;
    }
}
