package ar.com.flexia.challenge1.scms.services;

import ar.com.flexia.challenge1.global.dtos.APIConfirmation;
import ar.com.flexia.challenge1.scms.models.Ambiente;
import ar.com.flexia.challenge1.scms.models.Aplicacion;
import ar.com.flexia.challenge1.scms.models.Registro;
import ar.com.flexia.challenge1.scms.models.dtos.DeleteKeys;

import java.util.List;
import java.util.Map;

public interface scmsService {

    APIConfirmation setApp(Aplicacion app);

    APIConfirmation setEnv(Long appId, Ambiente env);

    APIConfirmation setConfig(Long envId, Map<String, String> config);

    APIConfirmation editConfig(Long envId, Map<String, String> config);

    APIConfirmation deleteConfig(Long envId, DeleteKeys campos);

    Map<String, String> getConfig(Long envId);
}
