package ar.com.flexia.challenge1.scms.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class ModifiedKeys {

    private List<String> campos;

    public ModifiedKeys() {
        campos = new ArrayList<>();
    }

    public List<String> getModificados() {
        return campos;
    }

    public void setModificados(List<String> campos) {
        this.campos = campos;
    }


    public void addModificado(String modificado) {
        this.campos.add(modificado);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return this.campos.isEmpty();
    }
}
