package ar.com.flexia.challenge1.scms.models;

import ar.com.flexia.challenge1.auth.models.APITokens;

import javax.persistence.*;

@Entity
public class Ambiente {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ambiente")
    private Long id;

    private String nombre;

    private String descripcion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "aplicacion.id")
    private Aplicacion aplicacion;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "apitokens.id")
    private APITokens tokens;

    public Ambiente() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Aplicacion getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(Aplicacion aplicacion) {
        this.aplicacion = aplicacion;
    }

    public APITokens getTokens() {
        return tokens;
    }

    public void setTokens(APITokens tokens) {
        this.tokens = tokens;
    }
}
