package ar.com.flexia.challenge1.scms.controllers;

import ar.com.flexia.challenge1.scms.models.Ambiente;
import ar.com.flexia.challenge1.scms.models.Registro;
import ar.com.flexia.challenge1.scms.models.dtos.DeleteKeys;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ar.com.flexia.challenge1.global.dtos.APIConfirmation;
import ar.com.flexia.challenge1.scms.models.Aplicacion;
import ar.com.flexia.challenge1.scms.services.scmsService;

import java.util.Map;


@RestController
@RequestMapping(path = "/api")
public class scmsController {

    @Autowired
    private scmsService scmsService;

    public scmsController() {}

    @Operation(summary = "Crear aplicación", description = "Permite registrar la aplicación de un cliente.", tags = {"Aplicaciones"})
    @PostMapping(path = "/apps")
    @Secured("ADMIN")
    public APIConfirmation setApp(@RequestBody Aplicacion app) {
        return scmsService.setApp(app);
    }

    @Operation(summary = "Crear ambiente", description = "Permite crear un ambiente para una aplicación dada.", tags = {"Aplicaciones"})
    @PostMapping(path = "/apps/{appId}/env")
    @Secured("ADMIN")
    public APIConfirmation setEnv(@PathVariable Long appId, @RequestBody Ambiente env) {
        return scmsService.setEnv(appId, env);
    }

    @Operation(summary = "Crear registros de configuración", description = "Permite crear los registros de configuración en un ambiente para una aplicación.", tags = {"Aplicacion - Configuración"})
    @PostMapping(path = "/apps/{appId}/env/{envId}/configuration")
    @Secured("ADMIN")
    @PreAuthorize("@APIVerification.checkAmbienteWithAplicacion(#envId, #appId)")
    public APIConfirmation setConfig(@PathVariable Long appId, @PathVariable Long envId, @RequestBody Map<String, String> config) {
        return scmsService.setConfig(envId, config);
    }

    @Operation(summary = "Editar registros de configuración", description = "Permite editar uno o varios registros de configuración existentes en un ambiente para una aplicación.", tags = {"Aplicacion - Configuración"})
    @PutMapping(path = "/apps/{appId}/env/{envId}/configuration")
    @Secured("ADMIN")
    @PreAuthorize("@APIVerification.checkAmbienteWithAplicacion(#envId, #appId)")
    public APIConfirmation editConfig(@PathVariable Long appId, @PathVariable Long envId, @RequestBody Map<String, String> config) {
        return scmsService.editConfig(envId, config);
    }

    @Operation(summary = "Eliminar registros de configuración", description = "Permite eliminar alguno o todos los registros de configuración en un ambiente para una aplicación.", tags = {"Aplicacion - Configuración"})
    @DeleteMapping(path = "/apps/{appId}/env/{envId}/configuration")
    @Secured("ADMIN")
    @PreAuthorize("@APIVerification.checkAmbienteWithAplicacion(#envId, #appId)")
    public APIConfirmation deleteConfig(@PathVariable Long appId, @PathVariable Long envId, @RequestBody DeleteKeys campos) {
        return scmsService.deleteConfig(envId, campos);
    }

    @Operation(summary = "Obtener configuración", description = "Devuelve todos los registros de configuración en un ambiente para una aplicación.", tags = {"Aplicacion - Configuración"})
    @GetMapping(path = "/apps/{appId}/env/{envId}/configuration")
    @PreAuthorize("@APIVerification.checkAmbienteWithAplicacion(#envId, #appId)")
    public Map<String, String> getConfig(@PathVariable Long appId, @PathVariable Long envId) {
        return scmsService.getConfig(envId);
    }
}
