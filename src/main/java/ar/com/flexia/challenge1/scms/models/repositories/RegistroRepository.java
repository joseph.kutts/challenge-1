package ar.com.flexia.challenge1.scms.models.repositories;

import ar.com.flexia.challenge1.scms.models.Ambiente;
import ar.com.flexia.challenge1.scms.models.Registro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegistroRepository extends JpaRepository<Registro, Long> {

    public List<Registro> findByAmbiente_Id(Long id);

    public void deleteAllByAmbiente_Id(Long id);

    public void deleteByKeyAndAmbiente_Id(String key, Long id);
}
