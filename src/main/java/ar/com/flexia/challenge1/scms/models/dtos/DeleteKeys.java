package ar.com.flexia.challenge1.scms.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class DeleteKeys {

    private List<String> campos;

    public DeleteKeys() {
        campos = new ArrayList<>();
    }

    public List<String> getCampos() {
        return campos;
    }

    public void setCampos(List<String> campos) {
        this.campos = campos;
    }

    @JsonIgnore
    public boolean isEmpty() {
        return this.campos.isEmpty();
    }
}
