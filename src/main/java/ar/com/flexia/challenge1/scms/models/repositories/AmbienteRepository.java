package ar.com.flexia.challenge1.scms.models.repositories;

import ar.com.flexia.challenge1.scms.models.Ambiente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AmbienteRepository extends JpaRepository<Ambiente, Long> {

}
