package ar.com.flexia.challenge1.global.dtos;

/**
 * Error general a nivel API
 */
public class APIError {
    private String code;
    private String message;
    private String stacktrace;

    /**
     * @param code
     * @param message
     * @param stacktrace
     */
    public APIError(String code, String message, String stacktrace) {
        this.code = code;
        this.message = message;
        this.stacktrace = stacktrace;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getStacktrace() {
        return stacktrace;
    }
}
