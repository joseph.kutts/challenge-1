package ar.com.flexia.challenge1.auth.models;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.*;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * User
 */
@Entity
public class User implements UserDetails {

	/**
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user")
	private Long id;
	
	private String name;

	@Email
	private String email;

	private String password;
	
	private String profile;

	/**
	 */
	public User() {}

	/**
	 * @param name
	 * @param email
	 * @param password
	 * @param profile
	 */
	public User(String name, String email, String password, String profile) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.profile = profile;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(new SimpleGrantedAuthority(this.profile));
	}

	@Override
	public String getUsername() {
		return this.email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the profile
	 */
	public String getProfile() {
		return profile;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
}
