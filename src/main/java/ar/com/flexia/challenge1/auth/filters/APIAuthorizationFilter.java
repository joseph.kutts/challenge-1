package ar.com.flexia.challenge1.auth.filters;

import ar.com.flexia.challenge1.auth.config.APIConfig;
import ar.com.flexia.challenge1.auth.models.APITokens;
import ar.com.flexia.challenge1.auth.models.repositories.APITokensRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.ObjectUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * {@link BasicAuthenticationFilter} thata verifies an API key
 */
public class APIAuthorizationFilter extends BasicAuthenticationFilter {

	private static final Logger LOG = LoggerFactory.getLogger(APIAuthorizationFilter.class);

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private APITokensRepository apiRepo;

	@Autowired
	private HttpMessageConverter<String> messageConverter;

	/**
	 * @param authenticationManager
	 */
	public APIAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// recupero contenido del header de auth
		String authToken = request.getHeader(APIConfig.KEY_HEADER_NAME);
		String authSecret = request.getHeader(APIConfig.SECRET_HEADER_NAME);

		if(!ObjectUtils.isEmpty(authToken) && !ObjectUtils.isEmpty(authSecret)) {
			// Lo revisamos en la base de datos
			Optional<APITokens> dbTokens = apiRepo.findByKey(authToken);

			if (dbTokens.isPresent() && encoder.matches(authSecret, dbTokens.get().getSecret())) {
				APITokens confirmedTokens = dbTokens.get();
				List<SimpleGrantedAuthority> auths = confirmedTokens.getAuthorities();

				// armamos elemento authnetication que contiene el identificador del usuario y los authorities o roles
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(confirmedTokens.getName(),
						null, auths);

				// pasamos auths al contexto de seguridad de Spring Framework
				SecurityContextHolder.getContext().setAuthentication(auth);

			} else {
				LOG.trace("API Key & Secret Verification failed");
				ServerHttpResponse outputMessage = new ServletServerHttpResponse(response);
				outputMessage.setStatusCode(HttpStatus.FORBIDDEN);
				messageConverter.write("La API Key o Secret ingresadas no son validas.", null, outputMessage);
			}
		}

		chain.doFilter(request, response);
	}
}
