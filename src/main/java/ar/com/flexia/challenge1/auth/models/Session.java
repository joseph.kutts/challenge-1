package ar.com.flexia.challenge1.auth.models;

public class Session {

	private String token;
	
	private User user;

	/**
	 * @param token
	 * @param user
	 */
	public Session(String token, User user) {
		this.token = token;
		this.user = user;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
}
