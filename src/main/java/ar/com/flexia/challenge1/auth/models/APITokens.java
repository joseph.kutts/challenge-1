package ar.com.flexia.challenge1.auth.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * User
 */
@Entity
public class APITokens {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_tokens")
	private Long id;

	private String name;

	private String key;

	private String secret;

	private String profile;

	/**
	 */
	public APITokens() {}

	public APITokens(String name, String key, String secret, String profile) {
		this.name = name;
		this.key = key;
		this.secret = secret;
		this.profile = profile;
	}

	/**
	 * @param key
	 * @param secret
	 */


	public List<SimpleGrantedAuthority> getAuthorities() {
		return Arrays.asList(new SimpleGrantedAuthority(this.profile));
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Long getId() {
		return id;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
}
