package ar.com.flexia.challenge1.auth.services.implementations;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ar.com.flexia.challenge1.auth.models.Session;
import ar.com.flexia.challenge1.auth.models.User;
import ar.com.flexia.challenge1.auth.models.repositories.UserRepository;
import ar.com.flexia.challenge1.auth.services.JWTService;
import ar.com.flexia.challenge1.auth.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	private PasswordEncoder encoder;
	
	private JWTService jwt;

	private UserRepository userRepo;


	/**
	 * @param encoder
	 * @param jwt
	 * @param
	 */
	public UserServiceImpl(PasswordEncoder encoder, JWTService jwt, UserRepository userRepo) {
		super();
		this.encoder = encoder;
		this.jwt = jwt;
		this.userRepo = userRepo;

		if (0 == this.userRepo.count()) {
			userRepo.save(new User("admin", "admin@admin.com", encoder.encode("admin"), "ADMIN"));
		}
	}

	@Override
	public Optional<User> findUser(String username) {
		return userRepo.findByEmail(username);
	}
	
	@Override
	public Session login(String email, String password) {
		User user = findUser(email).orElseThrow(() -> {
			return new UsernameNotFoundException("Usuario no encontrado.");
		});
		
		//compare passwords
		if (!encoder.matches(password, user.getPassword())) {
			throw new BadCredentialsException("Credenciales invalidas.");
		}
		
		String token = jwt.issueToken(user.getEmail(), Arrays.asList(user.getProfile()));

		return new Session(token, user);
	}

	@Override
	public Session register(User user) {
		Optional<User> repeated = userRepo.findByEmail(user.getEmail());

		if (repeated.isPresent()) {
			throw new BadCredentialsException("El email ingresado ya esta en uso.");
		}

		user.setPassword(encoder.encode(user.getPassword()));
		user.setProfile("ADMIN");

		userRepo.save(user);

		String token = jwt.issueToken(user.getEmail(), Arrays.asList(user.getProfile()));

		return new Session(token, user);
	}
}
