package ar.com.flexia.challenge1.auth.models.repositories;

import ar.com.flexia.challenge1.auth.models.APITokens;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface APITokensRepository extends JpaRepository<APITokens, Long> {

   public Optional<APITokens> findByKey(String key);

}
