package ar.com.flexia.challenge1.auth.models.repositories;

import java.util.Optional;

import ar.com.flexia.challenge1.auth.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	public Optional<User> findByEmail(String email);
}
