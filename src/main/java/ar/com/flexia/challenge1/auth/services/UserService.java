package ar.com.flexia.challenge1.auth.services;

import java.util.Optional;

import ar.com.flexia.challenge1.auth.models.Session;
import ar.com.flexia.challenge1.auth.models.User;

public interface UserService {

	public Optional<User> findUser(String username);

	public Session login(String email, String password);

	public Session register(User user);

}
